#include <Servo.h>

const int p_photo = 11; //servo qui appuie sur le bouton, 11 is pwm
const int p_lampe = 12; //contacteur lié à une lampe

Servo s_photo;

int pos = 0;             // variable permettant de conserver la position du servo
int angle_initial = 0;   //angle initial
int angle_final = 180;   //angle final
int increment = 1;       //incrément entre chaque position


void setup() {
  // put your setup code here, to run once:

  pinMode(p_lampe, OUTPUT);

  s_photo.attach(p_photo);  // attache le servo au pin spécifié sur l'objet myservo
  s_photo.write(angle_initial);
  

}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(p_lampe, HIGH);   
  delay(1000);                       // wait for a second
  
  for (int position = angle_initial; position <=angle_final; position ++){ 
        s_photo.write(position);  
        delay(15);  // on attend 15 millisecondes
    }

  for (int position = angle_final; position >=angle_initial; position --){ 
        s_photo.write(position);  
        delay(15);  
    }


  delay(1000);
    digitalWrite(p_lampe, LOW);    
  delay(10*1000*58-(180*15+180*15));                       // wait 10min-delay
}
